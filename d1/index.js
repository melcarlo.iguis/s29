const express = require('express');
const app = express();

const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));


// mock database
let users = [
{
	email : "nezukokamado@gmail.com",
	username:  "nezuko01",
	password : "letMeOut",
	isAdmin : false
},
{
	email : "tanjirokamado@gmail.com",
	username:  "gonpanchiro",
	password : "iAmTanjiro",
	isAdmin : false
},
{
	email : "zenitsuAgatsuma@gmail.com",
	username:  "zenitsuSleeps",
	password : "iNeedNezuko",
	isAdmin : true
}
]

let loggedUser ;

app.get('/', (req, res) => {
	res.send('Hello World')
})


app.get('/hello' , (req,res) => {
	res.send('Hello from Batch 131')
})

// POST Method
app.post('/' , (req,res) => {
	console.log(req.body);
	let x = req.body;
	res.send(`Hello I'm ${x.name} , I am ${x.age} years old . I could be describe as a ${x.description} `)
});


// Register route
app.post('/users/register', (req,res) =>{
	console.log(req.body);

	let newUser = {
		email : req.body.email,
		username: req.body.username,
		password : req.body.password,
		isAdmin : req.body.isAdmin	
	};

	users.push(newUser);
	console.log(users);
	res.send(`User ${req.body.username} has successfully registered.`)
})


// login route

app.post('/users/login', (req,res) => {
	console.log(req.body);

	let foundUser = users.find((user) =>{

		return user.username === req.body.username && user.password === req.body.password;

		});

		if(foundUser !== undefined){

			let foundUserIndex = users.findIndex((user) =>{

				return user.username === foundUser.username
			});
			foundUser.index = foundUserIndex;

			loggedUser = foundUser;

			console.log(loggedUser)

			res.send(`Thanks for logging in ${req.body.username}`)
		}
		else {
			loggedUser = foundUser;

			res.send('Login failed, wrong credentials.')
		}
});


// Change password route
app.put('/users/change-password' ,(req,res) =>{

	let message;

	for(let i = 0; i < users.length; i++){

	if(req.body.username === users[i].username){

		users[i].password = req.body.password;
		message = `User ${req.body.username}'s password has been changed`;

	break;
	}else{
		message = ('User not found')
	}
	}
	res.send(message)
})

// Create a GET route that will access the  /home route that will print out a simple message
app.get('/home' , (req,res) =>{
	res.send('Welcome to the homepage')
})


// Create a GET route that will access the /users route that will retrieve all the users in the mock database

app.get('/users' ,(req,res) => {

	console.log(users)
	res.send(users)
	
})


// Create a DELETE route that will access the /delete-user route to remove a user from the mock database

app.delete('/delete-user/:username' ,(req,res) =>{
	const {username} = req.params;

	const deleted = users.find(user => user.username === username)

	if(deleted){

		// kulang ng array.filter(userIndex ) mam hehehe
		// enter code here to delete data from array based on index no
		console.log('data has been deleted')
		res.send(`${deleted.username} has been deleted on the list`)
		console.log(users)
		
	}else{
		res.send("data not found, no data deleted")
	}
})

app.listen(port, () => console.log(`Server is running at port ${port}`));



